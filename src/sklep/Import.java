/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sklep;
import javax.swing.JFileChooser;
import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Operator
 */
public class Import extends javax.swing.JFrame {

    /**
     * Creates new form Import
     */
    
    static String strimie;
    static String strnazwisko;
    static JFileChooser plikwyb;
    
    public Import() {
        initComponents();
        setResizable(false);
        
        UIManager.put("FileChooser.saveButtonText", "Zapisz");
        UIManager.put("FileChooser.saveButtonToolTipText", "Zapisz wybrany plik.");
        UIManager.put("FileChooser.openButtonText", "Otwórz");
        UIManager.put("FileChooser.openButtonToolTipText", "Otwórz wybrany plik.");
        UIManager.put("FileChooser.cancelButtonText", "Anuluj");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Anuluj akcję.");
        UIManager.put("FileChooser.fileNameLabelText", "Nazwa Pliku:");
        UIManager.put("FileChooser.filesOfTypeLabelText", "Typ Pliku:");
        UIManager.put("FileChooser.lookInLabelText", "Szukaj w:");
        UIManager.put("FileChooser.saveInLabelText", "Zapisz w:");
        UIManager.put("FileChooser.newFolderToolTipText", "Utwórz nowy folder");
        UIManager.put("FileChooser.upFolderToolTipText","Do góry");
        UIManager.put("FileChooser.homeFolderToolTipText","Desktop");
        UIManager.put("FileChooser.listViewButtonToolTipText","Lista");
        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Szczegóły");
        UIManager.put("FileChooser.acceptAllFileFilterText","Wszystkie Pliki");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ImieImport = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        NazwiskoImport = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        sciezkanazwa = new javax.swing.JLabel();
        koniecimport = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Import");

        jLabel1.setText("<html><b>Podaj imie</b></html>");

        jLabel2.setText("<html><b>Podaj nazwisko</b><html>");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Wskaż scieżkę do pilku magazynu");

        jButton1.setText("Wybierz");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                plik(evt);
            }
        });

        sciezkanazwa.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        sciezkanazwa.setText("Scieżka...");

        koniecimport.setText("Zakończ");
        koniecimport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                koniecimportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(koniecimport)
                .addGap(49, 49, 49))
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sciezkanazwa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(NazwiskoImport, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(ImieImport)))
                        .addGap(0, 156, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ImieImport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(NazwiskoImport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(29, 29, 29)
                .addComponent(sciezkanazwa)
                .addGap(18, 18, 18)
                .addComponent(koniecimport)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void plik(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_plik
        // TODO add your handling code here:
        
        plikwyb = new JFileChooser();
        plikwyb.setDialogTitle("Wybierz plik...");
        FileNameExtensionFilter typ = new FileNameExtensionFilter("pliki tekstowe","txt");
        plikwyb.setFileFilter(typ);
        
        int zmienna = plikwyb.showOpenDialog(null);
        
        if(zmienna == JFileChooser.APPROVE_OPTION)
        {
            
            plikwyb.setDialogTitle("Wybór pliku");
            
        
            f = plikwyb.getSelectedFile();
        
            String sciezka = f.getAbsolutePath();
            sciezkanazwa.setText(sciezka);
        }
        else if(zmienna == JFileChooser.CANCEL_OPTION)
        {
            
        }
       
    }//GEN-LAST:event_plik

    private void koniecimportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_koniecimportActionPerformed
        // TODO add your handling code here:
        
        if( (ImieImport.getText().trim().isEmpty() || NazwiskoImport.getText().trim().isEmpty()) != true)
        {
            if(ImieImport.getText().charAt(0) >= '0' && ImieImport.getText().charAt(0) <= '9' || NazwiskoImport.getText().charAt(0) > '0' && NazwiskoImport.getText().charAt(0) <= '9')
            {
                JOptionPane.showMessageDialog(null,"Imie i nazwisko nie moze byc liczbą!", "Uwaga!", JOptionPane.ERROR_MESSAGE);
            }
            else
            {
                strimie=ImieImport.getText();
                strnazwisko=NazwiskoImport.getText();
                this.dispose();
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Imie i nazwisko nie moga byc puste!", "Uwaga!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_koniecimportActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Import.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Import.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Import.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Import.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Import().setVisible(true);
            }
        });
    }
    static File f;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ImieImport;
    private javax.swing.JTextField NazwiskoImport;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton koniecimport;
    private javax.swing.JLabel sciezkanazwa;
    // End of variables declaration//GEN-END:variables

   
}